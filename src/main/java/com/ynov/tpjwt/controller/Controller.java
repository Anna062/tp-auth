package com.ynov.tpjwt.controller;

import com.ynov.tpjwt.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class Controller {

    private UserService userService;

    @Autowired
    public Controller(UserService service){
        userService = service;
    }
    @PostMapping ("/login")
    public ResponseEntity<String> login(@RequestBody User user){
        if (user == null || user.getUsername() == null || user.getPassword() == null) {
            return ResponseEntity.badRequest().body("Nom d'utilisateur et mot de passe requis !");
        }
        if(UserService.authenticateUser(user)){
            final boolean isAdmin = userService.getUsersMap().get(user.getUsername());
            final String token = UserService.generateToken(user.getUsername(), isAdmin);
            userService.getTokens().replace(user.getUsername(), token);
            return ResponseEntity.ok(token);
        }
        if(userService.getTokens().containsKey(user.getUsername())){
            ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Vous avez déjà un token !");
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Échec de l'authentification !");
    }

    @GetMapping("/api/posts/{id}")
    public ResponseEntity<Post> getPost(@RequestHeader(value = "Authorization", required = false) String auth, @PathVariable int id) {
        if (auth == null || auth.isEmpty()) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        String token = auth.substring(7);
        User user = JwtMiddleware.getUserFromToken(token);

        if (user != null) {
            Post post = PostService.isAccess(user.getUsername(), id);
            if (post != null) {
                return ResponseEntity.ok(post);
            } else {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
            }
        }

        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

}
