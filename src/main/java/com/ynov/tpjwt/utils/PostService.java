package com.ynov.tpjwt.utils;

import java.util.Arrays;
import java.util.List;

public class PostService {

    private static List<Post> posts = Arrays.asList(new Post(456, "bob"),
            new Post(654, "alice")
            , new Post(555, "admin"));

    public static List<Post> getPosts() {
        return posts;
    }

    public static Post isAccess(String username, int postId){
        for(final Post post : posts){
            if(post.getAuthor().equals(username) && post.getId() == postId){

                return post;
            }
            if(username.equals("admin") && post.getId() == postId){
                return post;
            }
        }
        return null;
    }

}
