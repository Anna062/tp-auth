package com.ynov.tpjwt.utils;

import lombok.Getter;
import lombok.Setter;
@Setter
@Getter
public class User {

    private String username;

    private String password;
    @Setter
    private String token;

    public User(String username, String password){
        this.username = username;
        this.password = password;
        this.token = "";
    }
}
