package com.ynov.tpjwt.utils;

import lombok.Getter;

@Getter
public class Post {

    private int id;
    private String author;
    private String content;

    public Post(int id, String author){
        this.id = id;
        this.author = author;
        this.content = this.id + "-" + this.author;
    }
}
