package com.ynov.tpjwt.utils;

import org.springframework.stereotype.Service;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.*;

@Service
public class UserService {

    private static final long EXPIRATION_TIME = 24 * 60 * 60 * 1000;
    private static Map<String, Boolean> usersMap = Map.of("bob", false,
            "alice", false, "admin", true);
    private  Map<String, String> tokens = new HashMap<>();
    private static List<User> users = Arrays.asList(new User("admin", "admin"),
            new User("bob", "bob"),
            new User("alice", "alice"));

    public UserService(){
        this.tokens.put("bob", "");
        this.tokens.put("alice", "");
        this.tokens.put("admin", "");

    }

    public static String generateToken(String username, boolean isAdmin){
        Date now = new Date();
        Date expiration = new Date(now.getTime() + EXPIRATION_TIME);

        return Jwts.builder()
                .setSubject(username)
                .claim("role", isAdmin)
                .setExpiration(expiration)
                .signWith(SignatureAlgorithm.HS512, JwtMiddleware.getSecretKey())
                .compact();
    }

    public static boolean authenticateUser(User user){
        if(usersMap.containsKey(user.getUsername())){
            for(User u : users){
                if(u.getUsername().equals(user.getUsername()) && u.getPassword().equals(user.getPassword())){
                    return true;
                }
            }
        }
        return false;
    }
    public Map<String, Boolean> getUsersMap() {
        return usersMap;
    }

    public  Map<String, String> getTokens() {
        return tokens;
    }
}
