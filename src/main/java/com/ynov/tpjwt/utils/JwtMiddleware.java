package com.ynov.tpjwt.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;


public class JwtMiddleware implements HandlerInterceptor {

    private static final String SECRET_KEY = "8vz36yJw3/OLAWoVOy+pc88mselF7tWmLOYFb8dY4vAsM78c4bnLgUF6ckJXTn6xbS00TQi8WlA4ygk1LiGBwA==";
    private static final Logger logger = LoggerFactory.getLogger(JwtMiddleware.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("Authorization");

        if (token != null && token.startsWith("Bearer ")) {
            token = token.substring(7);

            try {
                Claims claims = Jwts.parserBuilder().setSigningKey(SECRET_KEY).build().parseClaimsJws(token).getBody();
                return true;
            } catch (Exception e) {
                logger.error("Erreur lors de la validation du token JWT : {}", e.getMessage());
            }
        }

        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        return false;
    }

    public static boolean isTokenValid(String token) {
        try {
            Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token);
            return true;
        } catch (JwtException | IllegalArgumentException e) {
            return false;
        }
    }

    public static User getUserFromToken(String token) {
        Jws<Claims> claimsJws = Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token);
        Claims claims = claimsJws.getBody();

        String userId = claims.getSubject();
        User user = new User("","");
        user.setUsername(userId);
        return user;
    }

    public static String getSecretKey() {
        return SECRET_KEY;
    }
}
